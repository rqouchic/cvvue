import { createWebHistory, createRouter } from "vue-router";


const routes = [
  {
    path: "/", /*Le chemin de l'URL où cette route peut être trouvée. */
    name: "Cv", /*Un nom facultatif à utiliser lorsque nous établissons un lien vers cette route. */
    component: () => import('../components/Cv.vue'), /*Quel composant charger lorsque cette route est appelée. */
  },
  {
      path: "/Photo-experience",
      name: "photo",
      component: () => import('../components/PhotoExperience.vue'),
  },

];

const router = createRouter({
    /* On crée le router et on change le mode Hash en mpde histoy.
    Le changement se voit dans l'URL.
    Hash se voit avec un #
    History correspond a une URL classique*/
  history: createWebHistory(),
  routes,
});

export default router; /*nous exportons ceci en bas car nous devrons l'importer dans notre fichier main.js. */